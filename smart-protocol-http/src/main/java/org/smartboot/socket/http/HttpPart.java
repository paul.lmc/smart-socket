/*
 * Copyright (c) 2018, org.smartboot. All rights reserved.
 * project name: smart-socket
 * file name: HttpPart.java
 * Date: 2018-01-23
 * Author: sandao
 */

package org.smartboot.socket.http;

/**
 * @author 三刀
 * @version V1.0 , 2017/8/30
 */
public enum  HttpPart {
    HEAD,BODY,END;
}
